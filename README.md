# carimam_cnn

Convolutionnal neural networks for humpback whale vocalisations and delphinid whistles.

Call either run_CNN_HB.py for humpback whales or run_CNN_delphi.py for delphinid whistles.

Call the python script followed by the list of wav files you would like to process, and optionnaly an output filename.
For example : 
`python run_CNN_HB.py file1.wav file2.wav -outfn predictions.pkl`

This script relies on torch, pandas, numpy, scipy, and tqdm to run. Install dependencies with pip or conda.
If a GPU and cuda are available on the current machine, process will run on GPU for faster computation.

paul.best@univ-tln.fr for more information
